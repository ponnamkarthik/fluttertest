
import 'package:flutter/material.dart';

class CustomColors {
  static Color primaryColor = const Color(0xFF319795);
  static Color secondaryColor = const Color(0xFF3182CE);

  static MaterialColor primaryMaterialColor = const MaterialColor(
    0xFF319795,
    {
      50: Color.fromRGBO(49, 151, 149, .1),
      100: Color.fromRGBO(49, 151, 149, .2),
      200: Color.fromRGBO(49, 151, 149, .3),
      300: Color.fromRGBO(49, 151, 149, .4),
      400: Color.fromRGBO(49, 151, 149, .5),
      500: Color.fromRGBO(49, 151, 149, .6),
      600: Color.fromRGBO(49, 151, 149, .7),
      700: Color.fromRGBO(49, 151, 149, .8),
      800: Color.fromRGBO(49, 151, 149, .9),
      900: Color.fromRGBO(49, 151, 149, 1),
    },
  );

  static Color l1 = const Color(0xFFEBF4FF);
  static Color l2 = const Color(0xFFE6FFFA);
  static Color lightPrimary = const Color(0xFF81E6D9);
  static Color borderColor = const Color(0xFFCBD5E0);
  static Color textColorDark = const Color(0xFF4A5568);
  static Color textColorNumber = const Color(0xFF718096);

}
