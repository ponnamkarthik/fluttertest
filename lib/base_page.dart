import 'package:flutter/material.dart';
import 'package:fluttertest/widgets/custom_appbar.dart';

class BasePage extends StatefulWidget {
  const BasePage({super.key, required this.child, this.scrollController});

  final Widget child;
  final ScrollController? scrollController;

  @override
  State<BasePage> createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 48,
            left: 0,
            right: 0,
            bottom: 0,
            child: widget.child,
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: CustomAppBar(
              scrollController: widget.scrollController,
            ),
          ),
        ],
      ),
    );
  }
}
