import 'package:flutter/material.dart';
import 'package:fluttertest/utils/custom_colors.dart';

class SliderOneWidget extends StatefulWidget {
  const SliderOneWidget({super.key});

  @override
  State<SliderOneWidget> createState() => _SliderOneWidgetState();
}

class _SliderOneWidgetState extends State<SliderOneWidget> {
  @override
  Widget build(BuildContext context) {
    bool isSmallScreen = MediaQuery.of(context).size.width < 700;
    if (isSmallScreen) {
      return _buildSmallScreen();
    }
    return _buildLargeScreen();
  }

  Widget _buildSmallScreen() {
    return Stack(
      children: [
        Positioned.fill(
          child: CustomPaint(
            painter: CurvedPainter(),
          ),
        ),
        Container(
          width: double.infinity,
          // decoration: BoxDecoration(
          //   gradient: LinearGradient(
          //     colors: [
          //       CustomColors.l1,
          //       CustomColors.l2,
          //     ],
          //   ),
          // ),
          child: Column(
            children: [
              const SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 20),
                child: Text(
                  "Deine Job\nwebsite",
                  style: Theme.of(context).textTheme.displayMedium?.copyWith(
                        color: const Color(0xFF2D3748),
                      ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                child: Image.asset(
                  "assets/images/hands.png",
                ),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildLargeScreen() {
    return Stack(
      children: [
        Positioned.fill(
          child: CustomPaint(
            painter: CurvedPainter(),
          ),
        ),
        Container(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 60,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 24, vertical: 20),
                    child: Column(
                      children: [
                        Text(
                          "Deine Job\nwebsite",
                          style: Theme.of(context)
                              .textTheme
                              .displayMedium
                              ?.copyWith(
                                color: const Color(0xFF2D3748),
                                fontSize: 65,
                                fontWeight: FontWeight.bold,
                              ),
                          textAlign: TextAlign.left,
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        InkWell(
                          onTap: () {},
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 54),
                            height: 32,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  CustomColors.primaryColor,
                                  CustomColors.secondaryColor,
                                ],
                              ),
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Center(
                              child: Text(
                                "Kostenlos Registrieren",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium
                                    ?.copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal,
                                    ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: Image.asset(
                      "assets/images/hands.png",
                      width: 300,
                      height: 300,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 60,
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class CurvedPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var rect = Offset.zero & size;
    var paint = Paint();
    paint.shader = LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        CustomColors.l1,
        CustomColors.l2,
      ],
    ).createShader(rect);
    var path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height);
    path.cubicTo(size.width * 0.2, size.height * 1.1,
        size.width * 0.8, size.height * 0.8, size.width, size.height - 60);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
