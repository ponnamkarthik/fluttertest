import 'package:flutter/material.dart';
import 'package:fluttertest/utils/custom_colors.dart';

class ContentItemOneWidget extends StatefulWidget {
  const ContentItemOneWidget({super.key, required this.content, required this.index});

  final Map<String, dynamic> content;
  final int index;

  @override
  State<ContentItemOneWidget> createState() => _ContentItemOneWidgetState();
}

class _ContentItemOneWidgetState extends State<ContentItemOneWidget> {
  @override
  Widget build(BuildContext context) {
    bool isSmallScreen = MediaQuery.of(context).size.width < 700;
    if(isSmallScreen) {
      return _buildSmallScreen();
    }
    return _buildLargeScreen();
  }

  Widget _buildSmallScreen() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  widget.content["image"],
                  width: 300,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                "${widget.index + 1}.",
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                  color: CustomColors.textColorNumber,
                  fontSize: 130,
                 fontWeight: FontWeight.normal,
                  textBaseline: TextBaseline.alphabetic,
                ),
                textAlign: TextAlign.end,
              ),
              const SizedBox(width: 12,),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    widget.content["title"],
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                      color: CustomColors.textColorNumber,
                      fontWeight: FontWeight.normal,
                    ),
                    textAlign: TextAlign.center,
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 12,
          ),
        ],
      ),
    );
  }

  Widget _buildLargeScreen() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 40),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                "${widget.index + 1}.",
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                  color: CustomColors.textColorNumber,
                  fontSize: 130,
                  fontWeight: FontWeight.normal,
                  textBaseline: TextBaseline.alphabetic,
                ),
                // style: Theme.of(context).textTheme.displayLarge?.copyWith(
                //   color: CustomColors.textColorNumber,
                //   fontSize: 130,
                //   fontWeight: FontWeight.w400,
                //   textBaseline: TextBaseline.alphabetic,
                // ),
                textAlign: TextAlign.end,
              ),
              const SizedBox(width: 12,),
              Container(
                constraints: const BoxConstraints(
                  maxWidth: 260,
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    widget.content["title"],
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                      color: CustomColors.textColorNumber,
                      fontWeight: FontWeight.normal,
                    ),
                    textAlign: TextAlign.left,
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(width: 24,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  widget.content["image"],
                  width: 300,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
