import 'package:flutter/material.dart';
import 'package:fluttertest/homepage/widgets/content_item_one_widget.dart';
import 'package:fluttertest/homepage/widgets/content_item_three_widget.dart';
import 'package:fluttertest/homepage/widgets/content_item_two_widget.dart';
import 'package:fluttertest/utils/custom_colors.dart';

class ContentWidget extends StatefulWidget {
  const ContentWidget({super.key});

  @override
  State<ContentWidget> createState() => _ContentWidgetState();
}

class _ContentWidgetState extends State<ContentWidget> {
  List<String> chips = ["Arbeitnehmer", "Arbeitgeber", "Temporärbüro"];
  int selectedChip = 0;

  List<List<Map<String, dynamic>>> content = [
    [
      {
        "title": "Erstellen dein Lebenslauf",
        "image": "assets/images/11.png",
      },
      {
        "title": "Erstellen dein Lebenslauf",
        "image": "assets/images/12.png",
      },
      {
        "title": "Mit nur einem Klick bewerben",
        "image": "assets/images/13.png",
      },
    ],
    [
      {
        "title": "Erstellen dein Lebenslauf",
        "image": "assets/images/11.png",
      },
      {
        "title": "Erstellen ein Jobinserat",
        "image": "assets/images/22.png",
      },
      {
        "title": "Wähle deinen neuen Mitarbeiter aus",
        "image": "assets/images/23.png",
      },
    ],
    [
      {
        "title": "Erstellen dein Lebenslauf",
        "image": "assets/images/11.png",
      },
      {
        "title": "Erhalte Vermittlungs- angebot von Arbeitgeber",
        "image": "assets/images/32.png",
      },
      {
        "title": "Vermittlung nach Provision oder Stundenlohn",
        "image": "assets/images/33.png",
      },
    ],
  ];

  @override
  Widget build(BuildContext context) {
    bool isSmallScreen = MediaQuery.of(context).size.width < 700;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(
          height: 40,
        ),
        Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                children: chips.map((e) {
                  int index = chips.indexOf(e);
                  return InkWell(
                    borderRadius: index == 0
                        ? const BorderRadius.only(
                            topLeft: Radius.circular(12),
                            bottomLeft: Radius.circular(12))
                        : index == 2
                            ? const BorderRadius.only(
                                topRight: Radius.circular(12),
                                bottomRight: Radius.circular(12))
                            : BorderRadius.circular(0),
                    onTap: () {
                      setState(() {
                        selectedChip = index;
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24, vertical: 12),
                      decoration: BoxDecoration(
                        color: selectedChip == index
                            ? CustomColors.lightPrimary
                            : Colors.white,
                        borderRadius: index == 0
                            ? const BorderRadius.only(
                                topLeft: Radius.circular(12),
                                bottomLeft: Radius.circular(12))
                            : index == 2
                                ? const BorderRadius.only(
                                    topRight: Radius.circular(12),
                                    bottomRight: Radius.circular(12))
                                : BorderRadius.circular(0),
                        border: Border.all(
                          color: CustomColors.borderColor,
                          width: 1,
                        ),
                      ),
                      child: Text(
                        e,
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                              color: selectedChip == index
                                  ? CustomColors.l2
                                  : CustomColors.primaryColor,
                              fontWeight: FontWeight.w500,
                            ),
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 32,
        ),
        Center(
            child: Text(
          "Drei einfache Schritte zu deinem neuen Job",
          style: Theme.of(context).textTheme.titleLarge?.copyWith(
                color: CustomColors.textColorDark,
                fontWeight: FontWeight.w600,
              ),
        )),
        const SizedBox(
          height: 24,
        ),
        Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ContentItemOneWidget(
                  content: content[selectedChip][0],
                  index: 0,
                ),
                ContentItemTwoWidget(
                  content: content[selectedChip][1],
                  index: 1,
                ),
                ContentItemThreeWidget(
                  content: content[selectedChip][2],
                  index: 2,
                ),
              ],
            ),
            if(!isSmallScreen)
            Positioned(
              top: 200,
              left: -100,
              right: 0,
              child: Image.asset("assets/images/arrow1.png", height: 200,),
            ),
            if(!isSmallScreen)
            Positioned(
              top: 500,
              left: 0,
              right: 100,
              child: Image.asset("assets/images/arrow2.png", height: 200,),
            ),
          ],
        ),
        const SizedBox(
          height: 24,
        ),
      ],
    );
  }
}
