import 'package:flutter/material.dart';
import 'package:fluttertest/utils/custom_colors.dart';

class ContentItemThreeWidget extends StatefulWidget {
  const ContentItemThreeWidget({super.key, required this.content, required this.index});

  final Map<String, dynamic> content;
  final int index;

  @override
  State<ContentItemThreeWidget> createState() => _ContentItemThreeWidgetState();
}

class _ContentItemThreeWidgetState extends State<ContentItemThreeWidget> {
  @override
  Widget build(BuildContext context) {

    bool isSmallScreen = MediaQuery.of(context).size.width < 700;
    if(isSmallScreen) {
      return _buildSmallScreen();
    }
    return _buildLargeScreen();
  }

  Widget _buildSmallScreen() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 20),
      decoration: const BoxDecoration(

      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          const SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                "${widget.index + 1}.",
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                  color: CustomColors.textColorNumber,
                  fontSize: 130,
                 fontWeight: FontWeight.normal,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(width: 12,),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    widget.content["title"],
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                      color: CustomColors.textColorNumber,
                      fontWeight: FontWeight.normal,
                    ),
                    textAlign: TextAlign.center,
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  widget.content["image"],
                  width: 300,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 12,
          ),
        ],
      ),
    );
  }

  Widget _buildLargeScreen() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 40),
      decoration: const BoxDecoration(

      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Text(
                "${widget.index + 1}.",
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                  color: CustomColors.textColorNumber,
                  fontSize: 130,
                  fontWeight: FontWeight.normal,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(width: 12,),
              Container(
                constraints: const BoxConstraints(
                  maxWidth: 260,
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    widget.content["title"],
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                      color: CustomColors.textColorNumber,
                      fontWeight: FontWeight.normal,
                    ),
                    textAlign: TextAlign.left,
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(width: 24,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  widget.content["image"],
                  width: 300,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
