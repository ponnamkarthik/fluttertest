import 'package:flutter/material.dart';
import 'package:fluttertest/utils/custom_colors.dart';
import 'dart:ui' as ui;

class ContentItemTwoWidget extends StatefulWidget {
  const ContentItemTwoWidget(
      {super.key, required this.content, required this.index});

  final Map<String, dynamic> content;
  final int index;

  @override
  State<ContentItemTwoWidget> createState() => _ContentItemTwoWidgetState();
}

class _ContentItemTwoWidgetState extends State<ContentItemTwoWidget> {
  @override
  Widget build(BuildContext context) {
    bool isSmallScreen = MediaQuery.of(context).size.width < 700;
    if (isSmallScreen) {
      return _buildSmallScreen();
    }
    return _buildLargeScreen();
  }

  Widget _buildSmallScreen() {
    return Stack(
      children: [
        Positioned.fill(
          child: CustomPaint(
            painter: CurvedItemPainterMobile(),
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 12,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                textBaseline: TextBaseline.alphabetic,
                children: [
                  Text(
                    "${widget.index + 1}.",
                    style: Theme.of(context).textTheme.displayLarge?.copyWith(
                          color: CustomColors.textColorNumber,
                          fontSize: 130,
                         fontWeight: FontWeight.normal,
                        ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Text(
                        widget.content["title"],
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                              color: CustomColors.textColorNumber,
                              fontWeight: FontWeight.normal,
                            ),
                        textAlign: TextAlign.center,
                        softWrap: true,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Image.asset(
                      widget.content["image"],
                      width: 300,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildLargeScreen() {
    return Stack(
      children: [
        Positioned.fill(
          child: CustomPaint(
            painter: CurvedItemPainter(),
          ),
        ),
        Column(
          children: [
            const SizedBox(
              height: 24,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 40),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Image.asset(
                          widget.content["image"],
                          width: 300,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    width: 24,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Text(
                        "${widget.index + 1}.",
                        style:
                            Theme.of(context).textTheme.displayLarge?.copyWith(
                                  color: CustomColors.textColorNumber,
                                  fontSize: 130,
                                 fontWeight: FontWeight.normal,
                                ),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Container(
                        constraints: const BoxConstraints(
                          maxWidth: 260,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Text(
                            widget.content["title"],
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge
                                ?.copyWith(
                                  color: CustomColors.textColorNumber,
                                  fontWeight: FontWeight.normal,
                                ),
                            textAlign: TextAlign.left,
                            softWrap: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class CurvedItemPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path = Path();
    path.moveTo(0, size.height);
    path.lineTo(0, size.height * 0.02);
    path.cubicTo(size.width * 0.2, size.height * 0.02, size.width * 0.3,
        size.height * 0.2, size.width * 0.5, size.height * 0.1);
    path.cubicTo(size.width * 0.7, size.height * 0.02, size.width * 0.8,
        size.height * -0.1, size.width, size.height * 0.2);
    path.lineTo(size.width, size.height * 0.7);
    path.cubicTo(size.width, size.height * 0.7, size.width * 0.7, size.height,
        size.width * 0.5, size.height);
    path.cubicTo(
        size.width * 0.3, size.height * 0.9, 0, size.height, 0, size.height);
    path.close();

    Paint paint = Paint()..style = PaintingStyle.fill;
    paint.shader = ui.Gradient.linear(
        Offset(size.width * 0.2103611, size.height * -307.4380),
        Offset(size.width * 0.6718995, size.height * -305.9917),
        [CustomColors.l1, CustomColors.l2],
        [0, 1]);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}


class CurvedItemPainterMobile extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path = Path();
    path.moveTo(0, size.height);
    path.lineTo(0, size.height * 0.02);
    path.cubicTo(size.width * 0.2, size.height * 0.02, size.width * 0.3,
        size.height * 0.15, size.width * 0.5, size.height * 0.1);
    path.cubicTo(size.width * 0.7, size.height * 0.02, size.width * 0.8,
        0, size.width, size.height * 0.05);

    path.lineTo(size.width, size.height * 0.9);
    path.cubicTo(size.width, size.height * 0.9, size.width * 0.7, size.height,
        size.width * 0.5, size.height);
    path.cubicTo(
        size.width * 0.3, size.height * 0.9, 0, size.height, 0, size.height * 1.02);
    path.close();

    Paint paint = Paint()..style = PaintingStyle.fill;
    paint.shader = ui.Gradient.linear(
        Offset(size.width * 0.2103611, size.height * -307.4380),
        Offset(size.width * 0.6718995, size.height * -305.9917),
        [CustomColors.l1, CustomColors.l2],
        [0, 1]);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
