import 'package:flutter/material.dart';
import 'package:fluttertest/utils/custom_colors.dart';

class CustomAppBar extends StatefulWidget {
  const CustomAppBar({super.key, this.scrollController});

  final ScrollController? scrollController;

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  bool showButton = false;

  @override
  void initState() {
    super.initState();
    widget.scrollController?.addListener(() {
      if (widget.scrollController!.offset > 350 && !showButton) {
        setState(() {
          showButton = true;
        });
      } else {
        if (showButton && widget.scrollController!.offset <= 350) {
          setState(() {
            showButton = false;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isSmallScreen = MediaQuery.of(context).size.width < 700;
    return Column(
      children: [
        Container(
          height: 5,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                CustomColors.primaryColor,
                CustomColors.secondaryColor,
              ],
            ),
          ),
        ),
        Container(
          height: 56,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.29),
                spreadRadius: 0,
                blurRadius: 6,
                offset: const Offset(0, 2),
              ),
            ],
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(8),
              bottomRight: Radius.circular(8),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              if (!isSmallScreen && showButton)
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                    ),
                  ),
                  onPressed: () {
                    print("Kostenlos Registrieren");
                  },
                  child: const Text("Kostenlos Registrieren"),
                ),
              const SizedBox(
                width: 20,
              ),
              TextButton(
                onPressed: () {
                  print("login");
                },
                child: const Text("Login"),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
