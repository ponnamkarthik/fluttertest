import 'package:flutter/material.dart';
import 'package:fluttertest/homepage/home_page.dart';
import 'package:fluttertest/utils/custom_colors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: CustomColors.primaryMaterialColor,
        fontFamily: 'Lato'
      ),
      home: const HomePage(),
    );
  }
}
